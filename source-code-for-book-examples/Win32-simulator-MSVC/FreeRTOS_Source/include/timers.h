/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    http://www.FreeRTOS.org/FAQHelp.html - 有问题吗? 阅读FAQ页面
    "我的应用不能运行, 有可能哪里出氏了?". 你定义了configASSERT()吗?

    http://www.FreeRTOS.org/support - 作为收到如此高质量软年的回馈，我们邀请你
    通过参与支持论坛，来支持我们的全球社区

    http://www.FreeRTOS.org/training - 投资于培训可以让你的团队尽可能早地提高生产力。
    现在，您可以直接从Real Time Engineers Ltd首席执行官Richard Barry
    那里获得FreeRTOS培训，他是全球领先RTO的权威机构。
    
    http://www.FreeRTOS.org/plus - 一系列FreeRTOS生态系统产品，
    包括FreeRTOS+Trace——一个不可或缺的生产力工具，一个与DOS兼容的FAT文件系统，
    以及我们的微型线程感知UDP/IP协议栈。

    http://www.FreeRTOS.org/labs - FreeRTOS新产品的孵化地。快来试试FreeRTOS+TCP，
    我们为FreeRTOS开发的新的开源TCP/IP协议栈。

    http://www.OpenRTOS.com - Real Time Engineers ltd.向High Integrity Systems ltd.
    授权FreeRTOS以OpenRTOS品牌销售。低成本的OpenRTOS许可证提供有票证的支持、赔偿和商业中间件。

    http://www.SafeRTOS.com - 高完整性系统还提供安全工程和独立SIL3认证版本，
    用于需要可证明可靠性的安全和关键任务应用。
*/


#ifndef TIMERS_H
#define TIMERS_H

#ifndef INC_FREERTOS_H
	#error "include FreeRTOS.h must appear in source files before include timers.h"
#endif

/*lint -e537 只有当应用程序代码碰巧也包含task.h时，才会多次包含这个头. */
#include "task.h"
/*lint +e537 */

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------
 * 宏与定义
 *----------------------------------------------------------*/

/* 可在计时器队列上发送/接收的命令的ID.
这些只能通过组成公共软件定时器API的宏来使用，如下所述.
从中断发送的命令必须使用最大的数字，因为tmrFIRST_FROM_ISR_COMMAND命令用于
确定是否应使用队列发送函数的任务或中断版本 */
#define tmrCOMMAND_EXECUTE_CALLBACK_FROM_ISR 	( ( BaseType_t ) -2 )
#define tmrCOMMAND_EXECUTE_CALLBACK				( ( BaseType_t ) -1 )
#define tmrCOMMAND_START_DONT_TRACE				( ( BaseType_t ) 0 )
#define tmrCOMMAND_START					    ( ( BaseType_t ) 1 )
#define tmrCOMMAND_RESET						( ( BaseType_t ) 2 )
#define tmrCOMMAND_STOP							( ( BaseType_t ) 3 )
#define tmrCOMMAND_CHANGE_PERIOD				( ( BaseType_t ) 4 )
#define tmrCOMMAND_DELETE						( ( BaseType_t ) 5 )

#define tmrFIRST_FROM_ISR_COMMAND				( ( BaseType_t ) 6 )
#define tmrCOMMAND_START_FROM_ISR				( ( BaseType_t ) 6 )
#define tmrCOMMAND_RESET_FROM_ISR				( ( BaseType_t ) 7 )
#define tmrCOMMAND_STOP_FROM_ISR				( ( BaseType_t ) 8 )
#define tmrCOMMAND_CHANGE_PERIOD_FROM_ISR		( ( BaseType_t ) 9 )


/**
 * 软定时器引用的类型. 比如，xTimerCreate()返回一个TimerHandle_t类型的变量，
 * 可用于其他的软定时器函数(例如:xTimerStart(), xTimerReset()).
 */
typedef void * TimerHandle_t;

/*
 * 定义定时器回调函数原型.
 */
typedef void (*TimerCallbackFunction_t)( TimerHandle_t xTimer );

/*
 * 定义xTimerPendFunctionCallFromISR()使用的函数原型 
 */
typedef void (*PendedFunction_t)( void *, uint32_t );

/**
 * TimerHandle_t xTimerCreate( 	const char * const pcTimerName,
 * 								TickType_t xTimerPeriodInTicks,
 * 								UBaseType_t uxAutoReload,
 * 								void * pvTimerID,
 * 								TimerCallbackFunction_t pxCallbackFunction );
 *
 * 创建一个新软定时器实例，并且返回一个可以引用的定时器句柄
 * FreeRTOS的实现中，软定时器使用了一定的内存，用来存储定时器数据结构。
 * 如果使用xTimerCreate创建定时器，这些内存在xTimerCreate内部自动动态分配。
 * (参考网址 http://www.freertos.org/a00111.html). 
 * 如果使用xTimerCreateStatic创建软定时器，应用的实现者要自己提供必要的内存。
 * 因此xTimerCreateStatic可以在没有动态内存分配的情况下创建软定时器
 *
 * 定时器是在休眠状态下创建的. xTimerStart(), xTimerReset(),
 * xTimerStartFromISR(), xTimerResetFromISR(), xTimerChangePeriod() 和
 * xTimerChangePeriodFromISR() 这些API函数可以把定时器转为活动状态
 *
 * @param pcTimerName 定时器的文本名称.名称纯粹是用于调试。
 * 内核只通过句柄引用定时器，从不使用它的名字。
 *
 * @param xTimerPeriodInTicks 定时器周期. 时间是以tick为周期的。常量portTICK_PERIOD_MS
 * 转换以毫秒为单位时间。例如，如果定时器必须在100个ticks后到期，那么xTimerPeriodInTicks
 * 必须设为100.或过生日，如果定时器必须500ms后到期，那么xPeriod可以设为(500 / portTICK_PERIOD_MS)
 * 前提示configTICK_RATE_HZ必须小于等于1000.
 *
 * @param uxAutoReload 如果uxAutoReload设为pdTRUE,那么定时器将按
 * xTimerPeriodInTicks参数指定的周期，反复的到期。
 * uxAutoReload 设置为 pdFALSE,那么定时器将单次触发，并且在到期后进入休眠状态。
 *
 * @param pvTimerID 分配给正在创建的计时器的标识符.主要是用于定时器回调函数，
 * 当同一个回调函数分配给了多个定时器时，用于区分回调。
 *
 * @param pxCallbackFunction 定时时间到期调用的函数.
 * 该函数的函数原型必须是TimerCallbackFunction_t类型,
 * "void vCallbackFunction( TimerHandle_t xTimer );".
 *
 * @return 创建成功，返回定时器的句柄,
 * 创建失败返回NULL(因为堆内存不够，或定时周期为0)
 *
 * 使用示例:
 * @verbatim
 * #define NUM_TIMERS 5
 *
 * // 一个数组用于持有创建的定时器.
 * TimerHandle_t xTimers[ NUM_TIMERS ];
 *
 * // 一个数组，用于记录每个定时器到期的次数
 * int32_t lExpireCounters[ NUM_TIMERS ] = { 0 };
 *
 * // 定义一个多个定时器共用的回调函数 
 * // 这个回调函数只记录定时器到期的次数，一旦定时器到期次数达到10次就停止它
 * void vTimerCallback( TimerHandle_t pxTimer )
 * {
 * int32_t lArrayIndex;
 * const int32_t xMaxExpiryCountBeforeStopping = 10;
 *
 * 	   // 如果定时器参数为NULL，可以选择性的做点啥.
 * 	   configASSERT( pxTimer );
 *
 *     // 那个定时器到期?
 *     lArrayIndex = ( int32_t ) pvTimerGetTimerID( pxTimer );
 *
 *     // 增加定时器到期次数
 *     lExpireCounters[ lArrayIndex ] += 1;
 *
 *     // 如果定时器到期次数超过10次，就停止它
 *     if( lExpireCounters[ lArrayIndex ] == xMaxExpiryCountBeforeStopping )
 *     {
 *         // 在定时器的回调函数中调用定时器API函数时不要使用阻塞时间，
 *		   //如果这样做的话，会导致死锁.
 *         xTimerStop( pxTimer, 0 );
 *     }
 * }
 *
 * void main( void )
 * {
 * int32_t x;
 *
 *     // 创建然后开启一些定时器。
 *	   // 在调度器开始之前开启定时器表示:一旦开启了调度器，就立即启动定时器
 *     
 *     for( x = 0; x < NUM_TIMERS; x++ )
 *     {
 *         xTimers[ x ] = xTimerCreate(    "Timer",       // 只是一个文本名称，内核不会使用.
 *                                         ( 100 * x ),   // 以ticks为单位的定时周期.
 *                                         pdTRUE,        // 定时器到期时，自动加载
 *                                         ( void * ) x,  // 给每个定时器一个唯一的id,将它做为数组的索引
 *                                         vTimerCallback // 每个定时器到期时都调用同一个回调函数
 *                                     );
 *
 *         if( xTimers[ x ] == NULL )
 *         {
 *             // 定时器没有创建成功
 *         }
 *         else
 *         {
 *             // 开启定时器，没有指定阻塞时间，即使指定时器也会被忽略，因为调度器还没有开启
 *             if( xTimerStart( xTimers[ x ], 0 ) != pdPASS )
 *             {
 *                 // 定时器可能没有激活
 *             }
 *         }
 *     }
 *
 *     // ...
 *     // 在这创建任务.
 *     // ...
 *
 *     // 启动调度器，将会启动定时器，因为它们已经准备好进入激话状态
 *     // 
 *     vTaskStartScheduler();
 *
 *     // 应该永远不会执行到这里
 *     for( ;; );
 * }
 * @endverbatim
 */
#if( configSUPPORT_DYNAMIC_ALLOCATION == 1 )
	TimerHandle_t xTimerCreate(	const char * const pcTimerName,
								const TickType_t xTimerPeriodInTicks,
								const UBaseType_t uxAutoReload,
								void * const pvTimerID,
								TimerCallbackFunction_t pxCallbackFunction ) PRIVILEGED_FUNCTION; /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
#endif

/**
 * TimerHandle_t xTimerCreateStatic(const char * const pcTimerName,
 * 									TickType_t xTimerPeriodInTicks,
 * 									UBaseType_t uxAutoReload,
 * 									void * pvTimerID,
 * 									TimerCallbackFunction_t pxCallbackFunction,
 *									StaticTimer_t *pxTimerBuffer );
 *
 * 创建一个新的软定时器实例，并返回引用该定时器的句柄 
 * 
 *
 * 创建一个新软定时器实例，并且返回一个可以引用的定时器句柄
 * FreeRTOS的实现中，软定时器使用了一定的内存，用来存储定时器数据结构。
 * 如果使用xTimerCreate创建定时器，这些内存在xTimerCreate内部自动动态分配。
 * (参考网址 http://www.freertos.org/a00111.html). 
 * 如果使用xTimerCreateStatic创建软定时器，应用的实现者要自己提供必要的内存。
 * 因此xTimerCreateStatic可以在没有动态内存分配的情况下创建软定时器
 *
 * 定时器是在休眠状态下创建的.  The xTimerStart(), xTimerReset(),
 * xTimerStartFromISR(), xTimerResetFromISR(), xTimerChangePeriod() and
 * xTimerChangePeriodFromISR() 这些API函数可以把定时器转为活动状态
 *
 *
 * @param pcTimerName 定时器的文本名称.名称纯粹是用于调试。
 * 内核只通过句柄引用定时器，从不使用它的名字。
 *
 * @param xTimerPeriodInTicks 定时器周期. 时间是以tick为周期的。常量portTICK_PERIOD_MS
 * 转换以毫秒为单位时间。例如，如果定时器必须在100个ticks后到期，那么xTimerPeriodInTicks
 * 必须设为100.或过生日，如果定时器必须500ms后到期，那么xPeriod可以设为(500 / portTICK_PERIOD_MS)
 * 前提示configTICK_RATE_HZ必须小于等于1000.
 *
 * @param uxAutoReload 如果uxAutoReload设为pdTRUE,那么定时器将按
 * xTimerPeriodInTicks参数指定的周期，反复的到期。
 * uxAutoReload 设置为 pdFALSE,那么定时器将单次触发，并且在到期后进入休眠状态。
 *
 * @param pvTimerID 分配给正在创建的计时器的标识符.主要是用于定时器回调函数，
 * 当同一个回调函数分配给了多个定时器时，用于区分回调。
 *
 * @param pxCallbackFunction 定时时间到期调用的函数.
 * 该函数的函数原型必须是TimerCallbackFunction_t类型,
 * "void vCallbackFunction( TimerHandle_t xTimer );".
 *
 * @param pxTimerBuffer 必面指向StaticTimer_t类型的定时器，
 *  后面将用它来保存软定时器的数据结构，免去了为期自动分配内存
 *
 * @return 创建成功返回定时器句柄，如果pxTimerBuffer是NULL,那么返回NULL
 *
 * 使用示例:
 * @verbatim
 *
 * // 用于保存软定时器的数据结构
 * static StaticTimer_t xTimerBuffer;
 *
 * // 一个定时器变量，将用于在定时器的回调函数中增加
 * UBaseType_t uxVariableToIncrement = 0;
 *
 * // 软定时器回调函数，增加软定时器创建成功后，传给它的参数，在第5次增加后，停止软定时器
 * static void prvTimerCallback( TimerHandle_t xExpiredTimer )
 * {
 * UBaseType_t *puxVariableToIncrement;
 * BaseType_t xReturned;
 *
 *     // 从定时器id获取变量地址� 译注(感着这里有点不对头)
 *     puxVariableToIncrement = ( UBaseType_t * ) pvTimerGetTimerID( xExpiredTimer );
 *
 *     // 增加变量值，表示定时器回调函数被执行
 *     ( *puxVariableToIncrement )++;
 *
 *     // 如果调用次数达到指定次数，则停止定时器
 *     // 
 *     if( *puxVariableToIncrement == 5 )
 *     {
 *         // 由于是在回调函数中调用，所以必须不能阻塞
 *         xTimerStop( xExpiredTimer, staticDONT_BLOCK );
 *     }
 * }
 *
 *
 * void main( void )
 * {
 *     // 创建软定时器.  xTimerCreateStatic()这个函数比xTimerCreate() 多一个参数
 *     // 这个参数是一个 StaticTimer_t类型结构的指针，它将持有软定时器的结构
 *	   // 如果这个参数传NULL，那么这个结构将自动分配，就与调用xTimerCreate函数一样了
 *     xTimer = xTimerCreateStatic( "T1",             // 名称，只用于调试，内核不使用
 *                                  xTimerPeriod,     // 定时器周期，以ticks为单位.
 *                                  pdTRUE,           // 表示自动加载定时器
 *                                  ( void * ) &uxVariableToIncrement,    // 在定时器回调函数中增加的变量
 *                                  prvTimerCallback, // 定时时间到函数执行
 *                                  &xTimerBuffer );  // 保存软定时器数据结构的内存
 *
 *     // 调度器还没有启动，所以阻塞时间无效
 *     xReturned = xTimerStart( xTimer, 0 );
 *
 *     // ...
 *     // 创建任务.
 *     // ...
 *
 *     // 启动调度器，将会启动定时器，因为它们已经准备好进入激话状态
 *     // 
 *     vTaskStartScheduler();
 *
 *     // 应该永远不会执行到这里
 *     for( ;; );
 * }
 * @endverbatim
 */
#if( configSUPPORT_STATIC_ALLOCATION == 1 )
	TimerHandle_t xTimerCreateStatic(	const char * const pcTimerName,
										const TickType_t xTimerPeriodInTicks,
										const UBaseType_t uxAutoReload,
										void * const pvTimerID,
										TimerCallbackFunction_t pxCallbackFunction,
										StaticTimer_t *pxTimerBuffer ) PRIVILEGED_FUNCTION; /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
#endif /* configSUPPORT_STATIC_ALLOCATION */

/**
 * void *pvTimerGetTimerID( TimerHandle_t xTimer );
 *
 * 返回赋给定时器的ID
 * ID是调用xTimerCreated()时通过参数pvTimerID赋值给定时器的，
 * 以及调用vTimerSetTimerID函数设置的
 *
 * 如果一个回调函数被赋给了多个定时器，那么定时器ID可用于储存特定的定时器
 *
 * @param xTimer 要查询的定时器.
 *
 * @return 返回定时器的ID.
 *
 * 使用示例:
 *
 * 参考 xTimerCreate() 函数的使用场景.
 */
void *pvTimerGetTimerID( const TimerHandle_t xTimer ) PRIVILEGED_FUNCTION;

/**
 * void vTimerSetTimerID( TimerHandle_t xTimer, void *pvNewID );
 *
 * 设置定时器的id.
 *
 * ID是调用xTimerCreated()时通过参数pvTimerID赋值给定时器的
 *
 * 如果一个回调函数被赋给了多个定时器，那么定时器ID可用于储存特定的定时器
 *
 * @param xTimer 要更新的定时器.
 *
 * @param pvNewID 给定时器的新ID.
 *
 * 使用示例:
 *
 * 参考 xTimerCreate() 函数的使用场景.
 */
void vTimerSetTimerID( TimerHandle_t xTimer, void *pvNewID ) PRIVILEGED_FUNCTION;

/**
 * BaseType_t xTimerIsTimerActive( TimerHandle_t xTimer );
 *
 * 查看定时器是激活状态或是休眠状态.
 *
 * 定时器将休眠，如果:
 *     1) 定时器已创建但还没有启动,或者
 *     2) 定时器是一个单次触发定时器，并且还没有重启
 *
 * 定时器创建后处理休眠状态.  The xTimerStart(), xTimerReset(),
 * xTimerStartFromISR(), xTimerResetFromISR(), xTimerChangePeriod() and
 * xTimerChangePeriodFromISR() 这些函数可将定时器转为激活状态
 *
 * @param xTimer 要查询的定时器.
 *
 * @return  定时器处理休眠状态返回pdFALSE.如果定时器是激活状态返回一个非pdFALSE值
 *
 * 使用示例:
 * @verbatim
 * // 该函数假定xTimer已经创建了
 * void vAFunction( TimerHandle_t xTimer )
 * {
 *     if( xTimerIsTimerActive( xTimer ) != pdFALSE ) // 或者更简洁的方式 "if( xTimerIsTimerActive( xTimer ) )"
 *     {
 *         // 定时器处于激活状态，做点啥
 *     }
 *     else
 *     {
 *         // 定时器没有激话，做点其他的
 *     }
 * }
 * @endverbatim
 */
BaseType_t xTimerIsTimerActive( TimerHandle_t xTimer ) PRIVILEGED_FUNCTION;

/**
 * TaskHandle_t xTimerGetTimerDaemonTaskHandle( void );
 *
 * 简单的返回定时器服务/守护任务的句柄.
 * 在调度器启动之前调用此函数xTimerGetTimerDaemonTaskHandle() 无效 
 */
TaskHandle_t xTimerGetTimerDaemonTaskHandle( void ) PRIVILEGED_FUNCTION;

/**
 * BaseType_t xTimerStart( TimerHandle_t xTimer, TickType_t xTicksToWait );
 *
 * 定时器的功能是通过定时器服务/守护任务实现的。许多公用的RTOS定时器API函数
 * 通过一个定时器命令队列，把命令发给定时器服务任务。这个定时器队列是内核私有的
 * 不能在应用层直接访问。命令队列的长度是由configTIMER_QUEUE_LENGTH配置决定的
 * 
 * xTimerStart()函数启动由xTimerCreate()创建的定时器。如果定时器已经启动并处于激活状态
 * 该函数的功能就相当于xTimerReset()
 * 启动一个定时器，并确保定时器进入激活状态。如果与此同时定时器没有停止，删除或复位。
 * 与其关连的回调函数将在xTimerReset()调用后n个ticks时被调用。这个n就是定时周期
 *
 * 在调度器启动之前调用xTimerStart()是可以的，但是这样的话，定时器实际上是直到调度器开始时才启动的.
 * 并且定时器的到期时间是相对于调度器启动时间，而不是相对于xTimerStart()的启动时间。
 *
 * 要使得xTimerStart()可用configUSE_TIMERS必须设置为1
 *
 * @param xTimer 启动或重启的定时器句柄 
 *
 * @param xTicksToWait 以tick为单位，当调用xTimerStart()时，当定时器任务的命令队列是满的时，
 * 调用任务需要阻塞多久，以等待命令成功发送到命令队列.在调用xTimerStart()时，
 * 若调度器还没有开启,则阻塞时间无效
 *
 * @return 如果等待的时间已到，但是命令还没有发送到定时器的命令队列就返回pdFAIL，
 * 如果命令成功发送到命令队列，将返回pdPASS.虽然定时器的过期时间，
 * 是相对于xTimerStart()实际调用的时间，但时命令实际被处理的时间，
 * 取决于定时服务任务相对于系统中其他任务的优先级。
 * 定时器任务的优先级是由常量configTIMER_TASK_PRIORITY配置的
 *
 * 使用示例:
 *
 * 参考 xTimerCreate() 函数的使用场景.
 *
 */
#define xTimerStart( xTimer, xTicksToWait ) xTimerGenericCommand( ( xTimer ), tmrCOMMAND_START, ( xTaskGetTickCount() ), NULL, ( xTicksToWait ) )

/**
 * BaseType_t xTimerStop( TimerHandle_t xTimer, TickType_t xTicksToWait );
 *
 * 定时器的功能是通过定时器服务/守护任务实现的。许多公用的RTOS定时器API函数
 * 通过一个定时器命令队列，把命令发给定时器服务任务。这个定时器队列是内核私有的
 * 不能在应用层直接访问。命令队列的长度是由configTIMER_QUEUE_LENGTH配置决定的
 * 
 * xTimerStop()停止之前使用xTimerStart(), xTimerReset(), xTimerStartFromISR(), xTimerResetFromISR(),
 * xTimerChangePeriod() 或者 xTimerChangePeriodFromISR() 函数启动的定时器。
 *
 * 停止一个定时器，确保它不是处理活动状态
 *
 * 要使得xTimerStop()可用configUSE_TIMERS必须设置为1
 *
 * @param xTimer 要停止的定时器句柄
 *
 * @param xTicksToWait 以tick为单位，当调用xTimerStop()时，当定时器任务的命令队列是满的时，
 * 调用任务需要阻塞多久，以等待命令成功发送到命令队列.在调用xTimerStop()时，
 * 若调度器还没有开启,则阻塞时间无效。
 *
 * @return pdFAIL 如果等待的时间已到，但是命令还没有发送到定时器的命令队列就返回pdFAIL，
 * 如果命令成功发送到命令队列，将返回pdPASS.命令实际被处理的时间，
 * 取决于定时服务任务相对于系统中其他任务的优先级。
 * 定时器任务的优先级是由常量configTIMER_TASK_PRIORITY配置的.
 *
 * 使用示例:
 *
 * 参考 xTimerCreate() 函数的使用场景.
 *
 */
#define xTimerStop( xTimer, xTicksToWait ) xTimerGenericCommand( ( xTimer ), tmrCOMMAND_STOP, 0U, NULL, ( xTicksToWait ) )

/**
 * BaseType_t xTimerChangePeriod( 	TimerHandle_t xTimer,
 *										TickType_t xNewPeriod,
 *										TickType_t xTicksToWait );
 *
 * 定时器的功能是通过定时器服务/守护任务实现的。许多公用的RTOS定时器API函数
 * 通过一个定时器命令队列，把命令发给定时器服务任务。这个定时器队列是内核私有的
 * 不能在应用层直接访问。命令队列的长度是由configTIMER_QUEUE_LENGTH配置决定的
 *
 * xTimerChangePeriod() 改变由xTimerCreate()函数创建的定时器的周期
 *
 * xTimerChangePeriod() 可以改变睡眠或活动状态定时器的周期 
 *  要使得xTimerChangePeriod()可用configUSE_TIMERS必须设置为1
 *
 * @param xTimer 要改变的定时器句柄 
 *
 * @param xNewPeriod 定时器的新周期. 定时器的周期是以tick为单位的。
 * 常量portTICK_PERIOD_MS用于转换毫秒为单位的时间。比如，如果定时器是100ticks到期，
 * 可以直接设 100.或者，如果是500ms到期，可以用( 500 / portTICK_PERIOD_MS )，前提是
 * configTICK_RATE_HZ的值少于或等于1000.
 *
 * @param xTicksToWait 以tick为单位，当调用xTimerChangePeriod()时，当定时器任务的命令队列是满的时，
 * 调用任务需要阻塞多久，以等待命令成功发送到命令队列.在调用xTimerChangePeriod()时，
 * 若调度器还没有开启,则阻塞时间无效。
 *
 * @return pdFAIL 如果等待的时间已到，但是命令还没有发送到定时器的命令队列就返回pdFAIL，
 * 如果命令成功发送到命令队列，将返回pdPASS.命令实际被处理的时间，
 * 取决于定时服务任务相对于系统中其他任务的优先级。
 * 定时器任务的优先级是由常量configTIMER_TASK_PRIORITY配置的.
 *
 * 使用示例:
 * @verbatim
 * // 此函数假定定时器已经创建.  当调用该函数时，如果xTimer指向的定时器已经处于激活状态，
 * // 那么删除定时器。如果还没有激活，那么将期周期改为500ms,然后启动它。
 * void vAFunction( TimerHandle_t xTimer )
 * {
 *     if( xTimerIsTimerActive( xTimer ) != pdFALSE ) // or more simply and equivalently "if( xTimerIsTimerActive( xTimer ) )"
 *     {
 *         // xTimer 已经激活，删除它
 *         xTimerDelete( xTimer );
 *     }
 *     else
 *     {
 *         // 定时器没有激活，改变它的周期为500ms. 这个操作会导致定时器启动.
 *		   // 如果命令不能立即发送到命令队列，则等待100个ticks
 *         if( xTimerChangePeriod( xTimer, 500 / portTICK_PERIOD_MS, 100 ) == pdPASS )
 *         {
 *             // 命令成功发送
 *         }
 *         else
 *         {
 *             // 命令在等待100ticks后，命令仍然没有发送到。在这里执行适合的操作
 *         }
 *     }
 * }
 * @endverbatim
 */
 #define xTimerChangePeriod( xTimer, xNewPeriod, xTicksToWait ) xTimerGenericCommand( ( xTimer ), tmrCOMMAND_CHANGE_PERIOD, ( xNewPeriod ), NULL, ( xTicksToWait ) )

/**
 * BaseType_t xTimerDelete( TimerHandle_t xTimer, TickType_t xTicksToWait );
 *
 * 定时器的功能是通过定时器服务/守护任务实现的。许多公用的RTOS定时器API函数
 * 通过一个定时器命令队列，把命令发给定时器服务任务。这个定时器队列是内核私有的
 * 不能在应用层直接访问。命令队列的长度是由configTIMER_QUEUE_LENGTH配置决定的
 * xTimerDelete()删除由xTimerCreate()函数创建的定时器的周期
 *
 * 要使得xTimerDelete()可用configUSE_TIMERS必须设置为1
 *
 * @param xTimer 定时器句柄
 *
 * @param xTicksToWait 以tick为单位，当调用xTimerDelete()时，当定时器任务的命令队列是满的时，
 * 调用任务需要阻塞多久，以等待命令成功发送到命令队列.在调用xTimerDelete()时，
 * 若调度器还没有开启,则阻塞时间无效。
 *
 * @return pdFAIL 如果等待的时间已到，但是命令还没有发送到定时器的命令队列就返回pdFAIL，
 * 如果命令成功发送到命令队列，将返回pdPASS.命令实际被处理的时间，
 * 取决于定时服务任务相对于系统中其他任务的优先级。
 * 定时器任务的优先级是由常量configTIMER_TASK_PRIORITY配置的.
 *
 * 使用示例:
 *
 * 参考 xTimerCreate() 函数的使用场景.
 */
#define xTimerDelete( xTimer, xTicksToWait ) xTimerGenericCommand( ( xTimer ), tmrCOMMAND_DELETE, 0U, NULL, ( xTicksToWait ) )

/**
 * BaseType_t xTimerReset( TimerHandle_t xTimer, TickType_t xTicksToWait );
 *
 * 定时器的功能是通过定时器服务/守护任务实现的。许多公用的RTOS定时器API函数
 * 通过一个定时器命令队列，把命令发给定时器服务任务。这个定时器队列是内核私有的
 * 不能在应用层直接访问。命令队列的长度是由configTIMER_QUEUE_LENGTH配置决定的
 * xTimerDelete()删除由xTimerCreate()函数创建的定时器的周期
 *
 * xTimerReset() 重启之前由xTimerCreate()创建的定时器,如果定时器已经启动并处于
 * 活动状态。xTimerReset() 将会导致重新计算它的到期时间，所以它的过期时间是相对于
 * xTimerReset() 的调用时间的。如果定时器处于睡眠状态，那么相当于调用xTimerStart()
 *
 * 重新启动一个定时器，并确保定时器进入激活状态。如果与此同时定时器没有停止，删除或复位。
 * 与其关连的回调函数将在xTimerReset()调用后n个ticks时被调用。这个n是定时周期
 *
 * 在调度器启动之前调用xTimerReset()是可以的，但是这样的话，定时器实际上是直到调度器开始时才启动的.
 * 并且定时器的到期时间是相对于调度器启动时间，而不是相对于xTimerReset()的启动时间。
 *
 * 要使得xTimerReset()可用configUSE_TIMERS必须设置为1 
 *
 * @param xTimer 定时器句柄
 *
 * @param xTicksToWait 以tick为单位，当调用xTimerReset()时，当定时器任务的命令队列是满的时，
 * 调用任务需要阻塞多久，以等待命令成功发送到命令队列.在调用xTimerReset()时，
 * 若调度器还没有开启,则阻塞时间无效。
 *
 * @return pdFAIL 如果等待的时间已到，但是命令还没有发送到定时器的命令队列就返回pdFAIL，
 * 如果命令成功发送到命令队列，将返回pdPASS.命令实际被处理的时间，
 * 取决于定时服务任务相对于系统中其他任务的优先级。
 * 定时器任务的优先级是由常量configTIMER_TASK_PRIORITY配置的.
 *
 * 使用示例:
 * @verbatim
 * // 当某个键按下时，一个LCD背光被点亮，如果5秒内没有任何键按下，那么关闭背光。
 * // 这个示例使用了单次触发定时器
 *
 * TimerHandle_t xBacklightTimer = NULL;
 *
 * // 回调函数被赋值给了单发定时器.这个例子不使用参数
 * void vBacklightTimerCallback( TimerHandle_t pxTimer )
 * {
 *     // 定时时间到，从一个键按下5秒时间到，关闭LCD背光
 *     vSetBacklightState( BACKLIGHT_OFF );
 * }
 *
 * // 按键事件处理器.
 * void vKeyPressEventHandler( char cKey )
 * {
 *     // 确保LCD点亮，然后复位定时器，它负责在5秒内没有按键按下，关闭背光。
 *     // 如果命令不能及时发送到成功就等待10ticks
 *     vSetBacklightState( BACKLIGHT_ON );
 *     if( xTimerReset( xBacklightTimer, 100 ) != pdPASS )
 *     {
 *         // 复位命令没有及时发送到。做相应的处理
 *     }
 *
 *     // 处理其他的按键
 * }
 *
 * void main( void )
 * {
 * int32_t x;
 *
 *     // 创建并启动一个单次触发定时器，用于负责5秒内没有任何按键按下时关闭背光
 *     xBacklightTimer = xTimerCreate( "BacklightTimer",           // 定时器名称，内核不使用
 *                                     ( 5000 / portTICK_PERIOD_MS), // 以ticks为单位的定时周期
 *                                     pdFALSE,                    // 表示单次触发定时器
 *                                     0,                          // 回调函数不使用这个id，所以可以随意设置
 *                                     vBacklightTimerCallback     // 关闭LCD背光的回调函数
 *                                   );
 *
 *     if( xBacklightTimer == NULL )
 *     {
 *         // 定时器没有创建
 *     }
 *     else
 *     {
 *         // 开启定时器，没有指定阻塞时间，即使指定时器也会被忽略，因为调度器还没有开启
 *         if( xTimerStart( xBacklightTimer, 0 ) != pdPASS )
 *         {
 *             // 定时没有处理激活状态
 *         }
 *     }
 *
 *     // 调度器还没有启动，所以阻塞时间无效
 *     xReturned = xTimerStart( xTimer, 0 );
 *
 *     // ...
 *     // 创建任务.
 *     // ...
 *
 *     // 启动调度器，将会启动定时器，因为它们已经准备好进入激话状态
 *     // 
 *     vTaskStartScheduler();
 *
 *     // 应该永远不会执行到这里
 *     for( ;; );
 * }
 * @endverbatim
 */
#define xTimerReset( xTimer, xTicksToWait ) xTimerGenericCommand( ( xTimer ), tmrCOMMAND_RESET, ( xTaskGetTickCount() ), NULL, ( xTicksToWait ) )

/**
 * BaseType_t xTimerStartFromISR( 	TimerHandle_t xTimer,
 *									BaseType_t *pxHigherPriorityTaskWoken );
 *
 * 可以在中断服务程序中调用的xTimerStart()版本
 *
 * @param xTimer 定时器句柄
 *
 * @param pxHigherPriorityTaskWoken 定时器服务/守护函数大部分时间都处理阻塞状态，
 * 等待来自命令队列表消息。调用xTimerStartFromISR()函数会写入一个消息到命令队列，
 * 所以就有可能把定时器服务/守护任务从阻塞态唤醒。如果调用xTimerStartFromISR()函数
 * 导致定时器服务/守护任务从阻塞态退出，并且定时器服务/守护任务的优先级等于或高于
 * 当前执行任务(被中断的任务)的优先级，那么在函数xTimerStartFromISR()的内部将会把
 * *pxHigherPriorityTaskWoken值设置为pdTRUE。如果*pxHigherPriorityTaskWoken值为pdTRUE，
 * 在退出中断前要进行上下文要换
 *
 * @return pdFAIL 如果等待的时间已到，但是命令还没有发送到定时器的命令队列就返回pdFAIL，
 * 如果命令成功发送到命令队列，将返回pdPASS.命令实际被处理的时间，
 * 取决于定时服务任务相对于系统中其他任务的优先级。
 * 定时器任务的优先级是由常量configTIMER_TASK_PRIORITY配置的.
 *
 * 使用示例:
 * @verbatim
 * // 该应用场景，假定xBacklightTimer已经创建。
 * // 当某个键按下时，一个LCD背光被点亮，如果5秒内没有任何键按下，那么关闭背光。
 * // 这个示例使用了单次触发定时器,与前面xTimerReset()函数不同的是按键处理程序是
 * // 一个中断服务程序
 *
 * // 回调函数被赋值给了单发定时器.这个例子不使用参数
 * void vBacklightTimerCallback( TimerHandle_t pxTimer )
 * {
 *     // 定时时间到，从一个键按下5秒时间到，关闭LCD背光
 *     vSetBacklightState( BACKLIGHT_OFF );
 * }
 *
 * // 按键中断服务程序.
 * void vKeyPressEventInterruptHandler( void )
 * {
 * BaseType_t xHigherPriorityTaskWoken = pdFALSE;
 *
 *     // 确保LCD点亮，然后复位定时器，它负责在5秒内没有按键按下，关闭背光。
 *     // 这是一个中断服务程序，所以只能调用以"FromISR"后缀的FreeRTOS API函数
 *     vSetBacklightState( BACKLIGHT_ON );
 *
 *     // xTimerStartFromISR() 或者 xTimerResetFromISR() 都可以在这里调用，
 *     // 这两个函数都会重新计算到期时间.
 *     // 本函数中 xHigherPriorityTaskWoken 初始化为pdFALSE。
 *     if( xTimerStartFromISR( xBacklightTimer, &xHigherPriorityTaskWoken ) != pdPASS )
 *     {
 *         // 开始命令没有成功发送，采取适当的动作
 *         // 
 *     }
 *
 *     // 执行其他按键处理
 *
 *     // 如xHigherPriorityTaskWoken 为pdTRUE.那么应执行上下文切换
 *     // 在中断函数中进行上下文切换的规则，因移植和编译器不同而不同。
 *     // 请到移植的示例中去查找实际使用的规则.
 *     if( xHigherPriorityTaskWoken != pdFALSE )
 *     {
 *         // 在这里调用中断安全的yeild函数 (实际的函数取决于FreeRTOS移植的实现).
 *         // 
 *     }
 * }
 * @endverbatim
 */
#define xTimerStartFromISR( xTimer, pxHigherPriorityTaskWoken ) xTimerGenericCommand( ( xTimer ), tmrCOMMAND_START_FROM_ISR, ( xTaskGetTickCountFromISR() ), ( pxHigherPriorityTaskWoken ), 0U )

/**
 * BaseType_t xTimerStopFromISR( 	TimerHandle_t xTimer,
 *									BaseType_t *pxHigherPriorityTaskWoken );
 *
 * 可以在中断服务程序中调用的xTimerStop()版本
 *
 * @param xTimer 定时器句柄
 *
 * @param pxHigherPriorityTaskWoken 定时器服务/守护函数大部分时间都处理阻塞状态，
 * 等待来自命令队列表消息。调用xTimerStopFromISR()函数会写入一个消息到命令队列，
 * 所以就有可能把定时器服务/守护任务从阻塞态唤醒。如果调用xTimerStopFromISR()函数
 * 导致定时器服务/守护任务从阻塞态退出，并且定时器服务/守护任务的优先级等于或高于
 * 当前执行任务(被中断的任务)的优先级，那么在函数xTimerStopFromISR()的内部将会把
 * *pxHigherPriorityTaskWoken值设置为pdTRUE。如果*pxHigherPriorityTaskWoken值为pdTRUE，
 * 在退出中断前要进行上下文要换 
 *
 * @return 如果等待的时间已到，但是命令还没有发送到定时器的命令队列就返回pdFAIL，
 * 如果命令成功发送到命令队列，将返回pdPASS.命令实际被处理的时间，
 * 取决于定时服务任务相对于系统中其他任务的优先级。
 * 定时器任务的优先级是由常量configTIMER_TASK_PRIORITY配置的.
 *
 * 使用示例:
 * @verbatim
 * // 这个场景假设定时器已经创建并启动。当中断发生时，只需停止该定时器
 * // 停止定时器的中断服务程序
 * void vAnExampleInterruptServiceRoutine( void )
 * {
 * BaseType_t xHigherPriorityTaskWoken = pdFALSE;
 *
 *     // 中断发生--停止定时器
 *     // xHigherPriorityTaskWoken 定义时为pdFALSE
 *     // 这是一个中断服务程序，所以只能调用以"FromISR"后缀的FreeRTOS API函数
 *     // 
 *     if( xTimerStopFromISR( xTimer, &xHigherPriorityTaskWoken ) != pdPASS )
 *     {
 *         // 开始命令没有成功发送，采取适当的动作
 *         //
 *     }
 *
 *     // 如xHigherPriorityTaskWoken 为pdTRUE.那么应执行上下文切换
 *     // 在中断函数中进行上下文切换的规则，因移植和编译器不同而不同。
 *     // 请到移植的示例中去查找实际使用的规则.
 *     if( xHigherPriorityTaskWoken != pdFALSE )
 *     {
 *         // 在这里调用中断安全的yeild函数 (实际的函数取决于FreeRTOS移植的实现).
 *         // 
 *     }
 * }
 * @endverbatim
 */
#define xTimerStopFromISR( xTimer, pxHigherPriorityTaskWoken ) xTimerGenericCommand( ( xTimer ), tmrCOMMAND_STOP_FROM_ISR, 0, ( pxHigherPriorityTaskWoken ), 0U )

/**
 * BaseType_t xTimerChangePeriodFromISR( TimerHandle_t xTimer,
 *										 TickType_t xNewPeriod,
 *										 BaseType_t *pxHigherPriorityTaskWoken );
 *
 * 可以在中断服务程序中调用的xTimerChangePeriod()版本 
 * 
 *
 * @param xTimer 定时器句柄
 *
 * @param xNewPeriod 定时器的新周期. 定时器的周期是以tick为单位的。
 * 常量portTICK_PERIOD_MS用于转换毫秒为单位的时间。比如，如果定时器是100ticks到期，
 * 可以直接设 100.或者，如果是500ms到期，可以用( 500 / portTICK_PERIOD_MS )，前提是
 * configTICK_RATE_HZ的值少于或等于1000.
 *
 * @param pxHigherPriorityTaskWoken 定时器服务/守护函数大部分时间都处理阻塞状态，
 * 等待来自命令队列表消息。调用xTimerChangePeriodFromISR()函数会写入一个消息到命令队列，
 * 所以就有可能把定时器服务/守护任务从阻塞态唤醒。如果调用xTimerChangePeriodFromISR()函数
 * 导致定时器服务/守护任务从阻塞态退出，并且定时器服务/守护任务的优先级等于或高于
 * 当前执行任务(被中断的任务)的优先级，那么在函数xTimerChangePeriodFromISR()的内部将会把
 * *pxHigherPriorityTaskWoken值设置为pdTRUE。如果*pxHigherPriorityTaskWoken值为pdTRUE，
 * 在退出中断前要进行上下文切换 
 *
 * @return 如果等待的时间已到，但是命令还没有发送到定时器的命令队列就返回pdFAIL，
 * 如果命令成功发送到命令队列，将返回pdPASS.命令实际被处理的时间，
 * 取决于定时服务任务相对于系统中其他任务的优先级。
 * 定时器任务的优先级是由常量configTIMER_TASK_PRIORITY配置的.
 *
 * 使用示例:
 * @verbatim
 * // 此场景假定定时器已经创建并启动. 
 * // 当中断发生时，那么将期周期改为500ms。
 *
 * // 改变定时器周期的中断服务函数
 * void vAnExampleInterruptServiceRoutine( void )
 * {
 * BaseType_t xHigherPriorityTaskWoken = pdFALSE;
 *
 *     // 中断发生--把定时器周期改为500ms
 *     // xHigherPriorityTaskWoken 定义时设为 pdFALSE
 *     // 这是一个中断服务程序，所以只能调用以"FromISR"后缀的FreeRTOS API函数
 *     // 
 *     if( xTimerChangePeriodFromISR( xTimer, &xHigherPriorityTaskWoken ) != pdPASS )
 *     {
 *         // 改变定时器周期的函数没有发送成功，采取适当的操作
 *         // 
 *     }
 *
 *     // 如xHigherPriorityTaskWoken 为pdTRUE.那么应执行上下文切换
 *     // 在中断函数中进行上下文切换的规则，因移植和编译器不同而不同。
 *     // 请到移植的示例中去查找实际使用的规则.
 *     if( xHigherPriorityTaskWoken != pdFALSE )
 *     {
 *         // 在这里调用中断安全的yeild函数 (实际的函数取决于FreeRTOS移植的实现).
 *         // 
 *     }
 * }
 * @endverbatim
 */
#define xTimerChangePeriodFromISR( xTimer, xNewPeriod, pxHigherPriorityTaskWoken ) xTimerGenericCommand( ( xTimer ), tmrCOMMAND_CHANGE_PERIOD_FROM_ISR, ( xNewPeriod ), ( pxHigherPriorityTaskWoken ), 0U )

/**
 * BaseType_t xTimerResetFromISR( 	TimerHandle_t xTimer,
 *									BaseType_t *pxHigherPriorityTaskWoken );
 *
 * xTimerResetFromISR 是可以从中断服务程序中调用的xTimerReset版本 
 *
 * @param xTimer 定时器句柄 
 *
 * @param pxHigherPriorityTaskWoken 定时器服务/守护函数大部分时间都处理阻塞状态，
 * 等待来自命令队列表消息。调用xTimerResetFromISR()函数会写入一个消息到命令队列，
 * 所以就有可能把定时器服务/守护任务从阻塞态唤醒。如果调用xTimerResetFromISR()函数
 * 导致定时器服务/守护任务从阻塞态退出，并且定时器服务/守护任务的优先级等于或高于
 * 当前执行任务(被中断的任务)的优先级，那么在函数xTimerResetFromISR()的内部将会把
 * *pxHigherPriorityTaskWoken值设置为pdTRUE。如果*pxHigherPriorityTaskWoken值为pdTRUE，
 * 在退出中断前要进行上下文要换
 *
 * @return 如果命令没有发送到定时器的命令队列，就返回pdFAIL，
 * 如果命令成功发送到命令队列，将返回pdPASS.命令实际被处理的时间，
 * 取决于定时服务任务相对于系统中其他任务的优先级，
 * 虽然过期时间是相对于实际xTimerResetFromISR()调用时间
 * 定时器任务的优先级是由常量configTIMER_TASK_PRIORITY配置的.
 *
 * 使用示例:
 * @verbatim
 * // 该应用场景，假定xBacklightTimer已经创建。
 * // 当某个键按下时，一个LCD背光被点亮，如果5秒内没有任何键按下，那么关闭背光。
 * // 这个示例使用了单次触发定时器,与前面xTimerReset()函数不同的是按键处理程序是
 * // 一个中断服务程序 
 *
 * // 回调函数被赋值给了单发定时器.这个例子不使用参数
 * // 
 * void vBacklightTimerCallback( TimerHandle_t pxTimer )
 * {
 *     // 定时时间到，从一个键按下5秒时间到，关闭LCD背光
 *     // 
 *     vSetBacklightState( BACKLIGHT_OFF );
 * }
 *
 * // 按键中断服务程序.
 * void vKeyPressEventInterruptHandler( void )
 * {
 * BaseType_t xHigherPriorityTaskWoken = pdFALSE;
 *
 *     // 确保LCD点亮，然后复位定时器，它负责在5秒内没有按键按下，关闭背光。
 *     // 这是一个中断服务程序，所以只能调用以"FromISR"后缀的FreeRTOS API函数
 *     vSetBacklightState( BACKLIGHT_ON );
 *
 *     // xTimerStartFromISR() 或者 xTimerResetFromISR() 都可以在这里调用，
 *     // 这两个函数都会重新计算到期时间.
 *     // 本函数中 xHigherPriorityTaskWoken 初始化为pdFALSE。
 *     if( xTimerResetFromISR( xBacklightTimer, &xHigherPriorityTaskWoken ) != pdPASS )
 *     {
 *         // 复位命令没有成功执行.采取适当的操作
 *         // 
 *     }
 *
 *     // 执行其他的按键处理
 *
 *     // 如xHigherPriorityTaskWoken 为pdTRUE.那么应执行上下文切换
 *     // 在中断函数中进行上下文切换的规则，因移植和编译器不同而不同。
 *     // 请到移植的示例中去查找实际使用的规则.
 *     if( xHigherPriorityTaskWoken != pdFALSE )
 *     {
 *         // 在这里调用中断安全的yeild函数 (实际的函数取决于FreeRTOS移植的实现).
 *         // 
 *     }
 * }
 * @endverbatim
 */
#define xTimerResetFromISR( xTimer, pxHigherPriorityTaskWoken ) xTimerGenericCommand( ( xTimer ), tmrCOMMAND_RESET_FROM_ISR, ( xTaskGetTickCountFromISR() ), ( pxHigherPriorityTaskWoken ), 0U )


/**
 * BaseType_t xTimerPendFunctionCallFromISR( PendedFunction_t xFunctionToPend,
 *                                          void *pvParameter1,
 *                                          uint32_t ulParameter2,
 *                                          BaseType_t *pxHigherPriorityTaskWoken );
 *
 * 用于在应用的中断服务程序，将函数推迟到 RTOS守护任务中执行(定时器服务任务，
 * 因此这个函数是在timers.c中实现的，并且前缀是Timer)
 *
 * 理想情况下一个中断服务程序应保持尽可能的短,但时很多时候ISR要么有很多事需要处理，
 * 要么执行不确定的处理。在这种情况下，该函数xTimerPendFunctionCallFromISR()
 * 可以把处理延迟到RTOS中的守护任务中去执行
 *
 * 提供一种机制，允许中断直接返回到后续执行挂起函数的任务，
 * 这允许回调函数最终执行--就好像该函数在中断中执行一样。
 *
 * @param xFunctionToPend 在定时器守护任务中执行的函数，函数的原型是PendedFunction_t
 *
 * @param pvParameter1 回调函数的第一个参数，这个参数是无类型指针类型，
  * 因此可以传任意类型，比如 unsigned long 可以转换为void* ,
  * 或者一个void*可以指向一个结构体
 *
 * @param ulParameter2 回调函数的第二个参数.
 *
 * @param pxHigherPriorityTaskWoken 如上所述，调用该函数将会导致一个消息发给定时器守
 * 护任务.如果守护任务的优先级(由常量configTIMER_TASK_PRIORITY配置指定)等于或高于当前
 * 执行任务的优先级(被中断的任务)，那么*pxHigherPriorityTaskWoken的值将在
 * xTimerPendFunctionCallFromISR()中被设置为pdTRUE。表示要在退出中断前进行上下文切换。
 * 正是这个原因，所以*pxHigherPriorityTaskWoken的值要初始化为pdFALSE。请看下面的示例
 *
 * @return 消息成功发送到定时器守护任务返回pdPASS ，否则返回 pdFALSE
 *
 * 示例:
 * @verbatim
 *
 *	// 这个函数将会在守护任务的上下文中执行，注意回调函数必须使用这个原型
 *  // 
 *  void vProcessInterface( void *pvParameter1, uint32_t ulParameter2 )
 *	{
 *		BaseType_t xInterfaceToService;
 *
 *		// 从服务程序中传过来的第二个参数，本例中第一个参数未使用
 *		xInterfaceToService = ( BaseType_t ) ulParameter2;
 *
 *		// 在这里执行处理
 *	}
 *
 *	// 一个中断服务程序，从多个接口中接收数据包
 *  void vAnISR( void )
 *	{
 *		BaseType_t xInterfaceToService, xHigherPriorityTaskWoken;
 *
 *		// 查询硬件，检查那个接口需要处理
 *		xInterfaceToService = prvCheckInterfaces();
 *
 *      // 实际的处理推迟到一个任务中,请求执行vProcessInterface()函数，
 *      // 并将接口的编号通过第二个参数传递过去，本示例第一个参数没有使用
 *		xHigherPriorityTaskWoken = pdFALSE;
 *		xTimerPendFunctionCallFromISR( vProcessInterface, NULL, ( uint32_t ) xInterfaceToService, &xHigherPriorityTaskWoken );
 *
 *		// 如果xHigherPriorityTaskWoken 现在为 pdTRUE 那么应进行上下文切换
 *		// portYIELD_FROM_ISR() or portEND_SWITCHING_ISR() 与移植相关,参考移植相关的文档
 *		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
 *
 *	}
 * @endverbatim
 */
BaseType_t xTimerPendFunctionCallFromISR( PendedFunction_t xFunctionToPend, void *pvParameter1, uint32_t ulParameter2, BaseType_t *pxHigherPriorityTaskWoken ) PRIVILEGED_FUNCTION;

 /**
  * BaseType_t xTimerPendFunctionCall( PendedFunction_t xFunctionToPend,
  *                                    void *pvParameter1,
  *                                    uint32_t ulParameter2,
  *                                    TickType_t xTicksToWait );
  * 用于将函数推迟到 RTOS守护任务中执行(定时器服务任务，
  * 因此这个函数是在timers.c中实现的，并且前缀是Timer)
  *
  * @param xFunctionToPend 在定时器守护任务中执行的函数
  * 这个函数必须是PendedFunction_t类型
  *
  * @param pvParameter1 回调函数的第一个参数，这个参数是无类型指针类型，
  * 因此可以传任意类型，比如 unsigned long 可以转换为void* ,
  * 或者一个void*可以指向一个结构体
  *
  * @param ulParameter2 回调函数的第二个参数.
  *
  * @param xTicksToWait 调用该函数将会导致发给一消息给定时器守护服务的队列。
  * xTicksToWait是指当定时器守护任务的队列满时，调用该函数的任务阻塞多久时间(即不占用CPU处理时间)
  *
  * @return 如果消息成功发送给了定时器守护任务返回pdPASS，否则返回pdFALSE
  *
  */
BaseType_t xTimerPendFunctionCall( PendedFunction_t xFunctionToPend, void *pvParameter1, uint32_t ulParameter2, TickType_t xTicksToWait ) PRIVILEGED_FUNCTION;

/**
 * const char * const pcTimerGetName( TimerHandle_t xTimer );
 *
 * 返回创建定时器时赋值的名称
 *
 * @param xTimer 要查询的定时器句柄 .
 *
 * @return 返回xTimer指定定时器的名称.
 */
const char * pcTimerGetName( TimerHandle_t xTimer ) PRIVILEGED_FUNCTION; /*lint !e971 Unqualified char types are allowed for strings and single characters only. */

/**
 * TickType_t xTimerGetPeriod( TimerHandle_t xTimer );
 *
 * 返回定时器的周期
 *
 * @param xTimer 要查询的定时器句柄 .
 *
 * @return 以 ticks为单位的定时周期.
 */
TickType_t xTimerGetPeriod( TimerHandle_t xTimer ) PRIVILEGED_FUNCTION;

/**
* TickType_t xTimerGetExpiryTime( TimerHandle_t xTimer );
*
* 返回计时器将过期的时间（以tick为单位）。
* 如果此值小于当前tick计数，则过期时间已从当前时间溢出。
* @param xTimer 要查询的定时器句柄 .
*
* @return 如果计时器正在运行，则返回计时器下次过期的时间（以tick为单位）。
* 如果计时器没有运行，则返回值未定义。
*/
TickType_t xTimerGetExpiryTime( TimerHandle_t xTimer ) PRIVILEGED_FUNCTION;

/*
 * 下面的函数不对外公开，仅用于内核内部
 */
BaseType_t xTimerCreateTimerTask( void ) PRIVILEGED_FUNCTION;
BaseType_t xTimerGenericCommand( TimerHandle_t xTimer, const BaseType_t xCommandID, const TickType_t xOptionalValue, BaseType_t * const pxHigherPriorityTaskWoken, const TickType_t xTicksToWait ) PRIVILEGED_FUNCTION;

#ifdef __cplusplus
}
#endif
#endif /* TIMERS_H */




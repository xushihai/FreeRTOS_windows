/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
	说明:
		创建两个任务，并动态修改两个任务的优先级
		以使得两个不同优先级的任务都能获得cpu,从而被执行
*/

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"

/* 示例包含头文件 */
#include "supporting_functions.h"

/* 两个任务函数声明 */
void vTask1( void *pvParameters );
void vTask2( void *pvParameters );

/* 用于保存task2任务的句柄 */
TaskHandle_t xTask2Handle;

/*-----------------------------------------------------------*/

int main( void )
{
	/* 创建第一个任务，并设置其优先级为2,
	这个任务的参数和句柄都不使用，置为NULL. */
	xTaskCreate( vTask1, "Task 1", 1000, NULL, 2, NULL );
						   /* 任务的优先级为 2 ^. */

	/* 创建第二个任务的优先级为1--它比第一个任务的优先级要低，
	该任务的参数也不用,但是我们用xTask2Handle保存任务的句柄(地址).*/
	xTaskCreate( vTask2, "Task 2", 1000, NULL, 1, &xTask2Handle );
						/* 任务句柄是最后一个参数 ^^^^^^^^^^^^^ */

	/* 开始任务调度使用地务被执行. */
	vTaskStartScheduler();	

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM.*/
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

void vTask1( void *pvParameters )
{
UBaseType_t uxPriority;

	/* 本任务由于它的优先级高于任务2,所以它将一直运行
	由于task1,task2都不会被阻塞，所以它们要么是运行态，要么就是就绪态.

	查询此任务的优先级 - 传递参数 NULL 的意思是：
	"返回自己的优级（即调用该函数的任务的优先级）". */
	uxPriority = uxTaskPriorityGet( NULL );

	for( ;; )
	{
		/* 打印任务名称. */
		vPrintString( "Task1 is running\r\n" );

		/* 设置task2的优先级高于task1的优先级，将导致task2立即执行
		（因为在两个创建的任务中，task2优先级较高）. */
		vPrintString( "About to raise the Task2 priority\r\n" );
		vTaskPrioritySet( xTask2Handle, ( uxPriority + 1 ) );

		/* 只有当Task1比Task2的优先级高时,Task1才会运行.
		因此，当本任务(Task1)运行到这里时，
		Task2必定是已经执行完成，并且将它自己的优先级设置为1了。
		(我们验证时可以在两个任务的，vTaskPrioritySet这一行加断点跟踪看一下情况)
		*/
		
	}
}

/*-----------------------------------------------------------*/

void vTask2( void *pvParameters )
{
UBaseType_t uxPriority;

	/* task1由于它的优先级高于任务2,所以它将一直运行
	由于task1,task2都不会被阻塞，所以它们要么是运行态，要么就是就绪态.

	查询此任务的优先级 - 传递参数 NULL 的意思是：
	"返回自己的优级（即调用该函数的任务的优先级）".
	注意:这里获取的优先级的值是，task1中为本任务设置的优先级，而不是本任务创建时的优先级
	这里的优先级值是3.
	*/
	uxPriority = uxTaskPriorityGet( NULL );

	for( ;; )
	{
		/* 
		当本任务运行到这里时，
		Task1必定已经运行，并且设置将本任务的优先级设为高于它自己的优先级		

		打印本任务的名字. */
		vPrintString( "Task2 is running\r\n" );

		/* 把自己的优先级还原.  第一个参数为NULL,表示改变自己的优先级.
		设置优先级低于Task1,使用Task1再次立即执行 */
		vPrintString( "About to lower the Task2 priority\r\n" );
		vTaskPrioritySet( NULL, ( uxPriority - 2 ) );
	}
}
/*-----------------------------------------------------------*/





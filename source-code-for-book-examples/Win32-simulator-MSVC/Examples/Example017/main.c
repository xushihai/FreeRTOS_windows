/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
	说明:
		本示例主要模拟演示了软中断的产生，
		以及使用记数信号量来进行中断服务函数和中断处理任务的同步		
*/

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* 示例包含头文件 */
#include "supporting_functions.h"


/* 本示例模拟中断号的使用。中断号0到2,用于FreeRTOS windows移植版系统使用。
所以第一个可以用的应用中断号是3 */
#define mainINTERRUPT_NUMBER	3

/* 要创建的任务 */
static void vHandlerTask( void *pvParameters );
static void vPeriodicTask( void *pvParameters );

/* 这个函数模拟中断服务函数。这就是那个任务要同步的中断。 */
static uint32_t ulExampleInterruptHandler( void );

/*-----------------------------------------------------------*/

/* 声明一个SemaphoreHandle_t类型的变量，用于指向(引用)任务与中断同步的信号量 */
SemaphoreHandle_t xCountingSemaphore;


int main( void )
{
    /* 在使用信号量之前，必须显式的创建它。
	   本例我们创建一个计数信号量，该信号量最大值是10,初始化为0. */
	xCountingSemaphore = xSemaphoreCreateCounting( 10, 0 );

	/* 检查信号量是否创建成功. */
	if( xCountingSemaphore != NULL )
	{
		/* 创建一个 'handler'（处理）任务，
		这个任务是中断延迟处理任务，该任务将与中断同步.
		该任务使用一个较高优先级来创建，是为了确保中断退出时，立即执行.
		本例使用了优先级3.  */
		xTaskCreate( vHandlerTask, "Handler", 1000, NULL, 3, NULL );

		/* 创建一个任务，用于周期性产生一个软中断。
		这个任务优先级低于处理任务的优先级，以确保当'handler'处理任务退出阻塞状态时，
		每次都能抢占cpu执行。 */
		xTaskCreate( vPeriodicTask, "Periodic", 1000, NULL, 1, NULL );

		/* 安装软中断处理函数句柄，这里的使用方式是基于FreeRTOS的移植实现。
		这种使用方式只是在FreeRTOS Windows上使用的，这里只是模拟中断 */
		vPortSetInterruptHandler( mainINTERRUPT_NUMBER, ulExampleInterruptHandler );

		/* 开启调度，使得任务被执行. */
		vTaskStartScheduler();
	}

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

static void vHandlerTask( void *pvParameters )
{
	/* 与大多数任务一样，本任务是一个无限循环  */
	for( ;; )
	{
		/* 使用一个信号量来等待事件发生。这个信号量在任务调度之前被创建，
		所以这里是任务运行时首次使用。
		该任务阻塞时间是永远(无穷),也就是说只有当获取信号量该函数才会返回，
		因此就不需要检查返回值。 */
		xSemaphoreTake( xCountingSemaphore, portMAX_DELAY );

		/* 当执行到这里时，事件必定已经发生。处理该事件
		(这里只是打印一个消息而已) */
		vPrintString( "Handler task - Processing event.\r\n" );
	}
}
/*-----------------------------------------------------------*/

static void vPeriodicTask( void *pvParameters )
{
const TickType_t xDelay500ms = pdMS_TO_TICKS( 500UL );

	/* 与大多数任务一样，本任务是一个无限循环  */
	for( ;; )
	{
		/* 该任务用于模拟一个中断，这里周期性的产生一个模拟的软中断。
		阻塞它直到再次产生软中断 */
		vTaskDelay( xDelay500ms );

		/* 产生一个中断，并在中断前后都输出一个消息，
		从消息的打印顺序来证明中断的执行顺序。

		这里产生软中断的方式与FreeRTOS的移植实现相关。
		这里的使用方式只适用于FreeRTOS的windows移植版本。
		这里只能是模拟产生中断. */
		vPrintString( "Periodic task - About to generate an interrupt.\r\n" );
		vPortGenerateSimulatedInterrupt( mainINTERRUPT_NUMBER );
		vPrintString( "Periodic task - Interrupt generated.\r\n\r\n\r\n" );
	}
}
/*-----------------------------------------------------------*/

static uint32_t ulExampleInterruptHandler( void )
{
BaseType_t xHigherPriorityTaskWoken;

	/* xHigherPriorityTaskWoken 这个参数开始要初始化为pdFALSE
	因为，如果要进行上下文切换，在中断的安全的API函数中，会将该变量置为pdTRUE. */
	xHigherPriorityTaskWoken = pdFALSE;

	/* 'Give' 给信号量多次。第一句给出的信号量，将会使得中断处理任务退出阻塞
	态，接下来给出的两次信号量，演示信号量会锁存产生的事件，从而让处理任务轮
	流处理这些中断事件，而不会丢失事件。这里模拟了多个中断被处理者处理的情况，
	虽然这里的事件是只是单次中断产生的。*/
	xSemaphoreGiveFromISR( xCountingSemaphore, &xHigherPriorityTaskWoken );
	xSemaphoreGiveFromISR( xCountingSemaphore, &xHigherPriorityTaskWoken );
	xSemaphoreGiveFromISR( xCountingSemaphore, &xHigherPriorityTaskWoken );

	/* 把xHigherPriorityTaskWoken的值传给portYIELD_FROM_ISR()函数。
	如果xHigherPriorityTaskWoken的值在xSemaphoreGiveFromISR()中被置为pdTRUE了。
	那么调用portYIELD_FROM_ISR()该函数，将会导致请求上下文切换。
	如果xHigherPriorityTaskWoken的值是pdFALSE,
	那么portYIELD_FROM_ISR()的调用将没有任务效果（即不产生任何影响）。

	在windows中，该函数的portYIELD_FROM_ISR()实现，包含返回语句。
	所以这里就不显式返回一个值了。
	（我们查看portYIELD_FROM_ISR()源码，实际就是一个return语句。） */
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}










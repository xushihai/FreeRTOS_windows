/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
	说明:
		创建一个低优先级的任务task1,
		并在task1中创建一个高先先级任务task2，
		task2中将自己删除。    
*/

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"

/* 示例包含头文件 */
#include "supporting_functions.h"

/* 任务函数声明 */
void vTask1( void *pvParameters );
void vTask2( void *pvParameters );

/* 用于持有Task2任务句柄. */
TaskHandle_t xTask2Handle;

/*-----------------------------------------------------------*/

int main( void )
{
	/* 创建第一个优先级为1的任务，它的参数和任务句柄都不需要使用，传NULL*/
	xTaskCreate( vTask1, "Task 1", 1000, NULL, 1, NULL );
							   /* 任务的优先级 ^. */

	/* 开启调度，使得任务被执行. */
	vTaskStartScheduler();	

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

void vTask1( void *pvParameters )
{
const TickType_t xDelay100ms = pdMS_TO_TICKS( 100UL );

	for( ;; )
	{
		/* 打印任务名称. */
		vPrintString( "Task1 is running\r\n" );

		/* 创建优先级较高的任务2,该任务的参数不用，置为NULL,
		但是我们用xTask2Handle保存任务的句柄(地址).*/
		xTaskCreate( vTask2, "Task 2", 1000, NULL, 2, &xTask2Handle );
							/* 任务句柄是最后一个参数 ^^^^^^^^^^^^^ */

		/* Task2的优先先级更高,所以Task1 运行到这里时， Task2必定运行完，并把自己删除了.
		延迟100ms */
		vTaskDelay( xDelay100ms );
	}
}

/*-----------------------------------------------------------*/

void vTask2( void *pvParameters )
{
	/* Task2 把自己删除. Task2要删除自己可以用vTaskDelete()，传一个NULL参数实现
	但是这里为了演示，我们使用xTask2Handle这个任务句柄来删除它自己 */
	vPrintString( "Task2 is running and about to delete itself\r\n" );
	vTaskDelete( xTask2Handle );
}
/*-----------------------------------------------------------*/




